use anyhow::Context;
use anyhow::Result;
use bigdecimal::BigDecimal;
use regex::Regex;
use serde_derive::Deserialize;
use std::convert::TryFrom;
use std::path::Path;
use std::str::FromStr;

#[derive(Deserialize, Debug)]
pub struct RawConfig {
    xlsx: RawXlsxConfig,
    numbers: NumbersConfig,
    categories: Vec<RawCategoryConfig>,
}

#[derive(Debug)]
pub struct Config {
    pub xlsx: XlsxConfig,
    pub numbers: NumbersConfig,
    pub categories: Vec<Category>,
}

impl Config {
    pub fn read_from_file<P: AsRef<Path>>(config_path: P) -> Result<Config> {
        let str = std::fs::read_to_string(config_path)
            .with_context(|| "could not read config file moneypie.toml")?;
        let rc: RawConfig =
            toml::from_str(&str).with_context(|| "could not parse config as toml")?;
        Config::try_from(rc)
    }
}

impl TryFrom<RawConfig> for Config {
    type Error = anyhow::Error;

    fn try_from(raw: RawConfig) -> Result<Self, Self::Error> {
        let xlsx = XlsxConfig::try_from(raw.xlsx)?;
        let categories = {
            let mut res = Vec::with_capacity(raw.categories.len());
            for rc in raw.categories {
                let c = Category::try_from(rc)?;
                res.push(c)
            }
            res
        };
        Ok(Config {
            xlsx,
            numbers: raw.numbers,
            categories,
        })
    }
}

#[derive(Deserialize, Debug)]
pub struct RawXlsxConfig {
    sheet: String,
    skip_rows: usize,
    description_column: usize,
    amount_out_column: usize,
    amount_in_column: usize,
    // TODO: amount_column // for single column with +/- values
}

impl TryFrom<RawXlsxConfig> for XlsxConfig {
    type Error = anyhow::Error;

    fn try_from(raw: RawXlsxConfig) -> Result<Self, Self::Error> {
        let res = XlsxConfig {
            sheet: raw.sheet,
            skip_rows: raw.skip_rows,
            description_column: raw.description_column,
            amount: AmountConfig::TwoColumns {
                in_column: raw.amount_in_column,
                out_column: raw.amount_out_column,
            },
        };
        Ok(res)
    }
}

#[derive(Debug)]
pub struct XlsxConfig {
    pub sheet: String,
    pub skip_rows: usize,
    pub description_column: usize,
    pub amount: AmountConfig,
}

#[derive(Debug)]
pub enum AmountConfig {
    TwoColumns { in_column: usize, out_column: usize },
}

#[derive(Deserialize, Debug)]
pub struct NumbersConfig {
    pub separator_thousands: String,
    pub separator_decimal: String,
}

impl NumbersConfig {
    pub fn parse_amount(&self, input: &str) -> Result<BigDecimal> {
        let input = input.replace(&self.separator_thousands, "");
        let input = input.replace(&self.separator_decimal, ".");
        Ok(BigDecimal::from_str(&input.to_string())?)
    }
}

#[derive(Deserialize, Debug)]
struct RawCategoryConfig {
    name: String,
    ignore: Option<bool>,
    color: Option<String>,
    description_regex: Vec<String>,
}

#[derive(Debug, Clone)]
pub struct Category {
    ignore: bool,
    pub name: String,
    pub color: Option<String>,
    description_regex: Vec<Regex>,
}

impl Category {
    pub fn description_matches(&self, desc: &str) -> bool {
        for re in &self.description_regex {
            if desc.contains(re) {
                return true;
            }
        }
        return false;
    }

    pub fn find_matching_category<'a>(
        config: &'a Config,
        description: &str,
    ) -> Option<&'a Category> {
        config
            .categories
            .iter()
            .find(|c| c.description_matches(description))
    }
}

impl TryFrom<RawCategoryConfig> for Category {
    type Error = anyhow::Error;

    fn try_from(raw: RawCategoryConfig) -> Result<Self, Self::Error> {
        Ok(Category {
            ignore: raw.ignore.unwrap_or(false),
            color: raw.color,
            name: raw.name.clone(),
            description_regex: {
                let mut res = Vec::new();
                for s in &raw.description_regex {
                    res.push(Regex::new(s)?)
                }
                res
            },
        })
    }
}
