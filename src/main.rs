use std::collections::HashMap;
use std::convert::{From, TryFrom};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use anyhow::{Context, Error, Result};
use bigdecimal::{BigDecimal, ToPrimitive};
use calamine::{DataType, Reader, Xlsx};
use itertools::Itertools;
use lazy_static::lazy_static;
use piechart::{Chart, Data};
use regex::Regex;
use serde::__private::fmt::Debug;
use structopt::StructOpt;
use term_table::table_cell::TableCell;

use moneypie::config::*;

lazy_static! {
    static ref SPACE_RE: Regex = Regex::new("\\s+").unwrap();
}

#[derive(Debug, StructOpt)]
#[structopt(name = "moneypie", about = "categorize expenses and chart them")]
struct Opt {
    /// Set the config profile to be used
    #[structopt(default_value = "bp")]
    profile: String,

    /// Sets the input file
    #[structopt(parse(from_os_str))]
    input_file_path: PathBuf,
}

fn main() -> Result<()> {
    // print_excel(&conf)
    // print_pie_chart();
    let config = Config::read_from_file("moneypie.toml")?;
    let workbook_path = PathBuf::from_str("bp.xlsx")?;
    let categorized_amounts = CategorizedAmount::read_from_xlsx(&config, &workbook_path)?;
    let table = CategorizedAmounts::build_table(&categorized_amounts);
    println!("{}", table.render());

    let categorized_amounts = CategorizedAmounts::from(&categorized_amounts[..]);
    let piechart_data = categorized_amounts.build_pie_chart_data();
    Chart::new()
        .radius(15)
        .aspect_ratio(5)
        .legend(true)
        .draw(&piechart_data[..]);

    Ok(())
}

#[derive(Debug, Eq, PartialEq)]
pub enum Direction {
    In,
    Out,
}

#[derive(Debug)]
pub struct CategorizedAmount<'a> {
    amount: BigDecimal,
    direction: Direction,
    category: &'a Category,
    description: String,
}

impl<'a> CategorizedAmount<'_> {
    pub fn read_from_xlsx<P>(
        config: &'a Config,
        workbook_name: &P,
    ) -> Result<Vec<CategorizedAmount<'a>>>
    where
        P: AsRef<Path> + Debug,
    {
        let mut workbook: Xlsx<_> = calamine::open_workbook(workbook_name)
            .with_context(|| format!("could not open workbook {:?}", workbook_name))?;
        let sheet_name = &config.xlsx.sheet;
        let range = workbook
            .worksheet_range(sheet_name)
            .with_context(|| format!("could not read worksheet {}", sheet_name))??;
        let rows = range.rows().skip(config.xlsx.skip_rows);
        let mut res = Vec::with_capacity(rows.len());
        for row in rows {
            let description = CategorizedAmount::parse_description(config, &row)?;
            let category = Category::find_matching_category(&config, &description)
                .with_context(|| format!("could not get category_name from row {:#?}", &row))?;
            let (direction, amount) = CategorizedAmount::parse_amount(&config, &row)?;
            let ca = CategorizedAmount {
                direction,
                amount,
                category,
                description,
            };
            res.push(ca)
        }
        Ok(res)
    }
    fn parse_amount(config: &Config, row: &[DataType]) -> Result<(Direction, BigDecimal)> {
        match &config.xlsx.amount {
            AmountConfig::TwoColumns {
                in_column,
                out_column,
            } => {
                use calamine::DataType::*;
                match (&row[*in_column], &row[*out_column]) {
                    (Empty, Empty) => {
                        unimplemented!("return error")
                    }
                    (Empty, String(s)) => {
                        let amt = config.numbers.parse_amount(s).with_context(|| {
                            format!(
                                "failed to parse out amount from cell String({}). row={:#?}",
                                s, &row
                            )
                        })?;
                        Ok((Direction::Out, amt.abs()))
                    }
                    (String(s), Empty) => {
                        let amt = config.numbers.parse_amount(s).with_context(|| {
                            format!(
                                "failed to parse in amount from cell String({}). row={:#?}",
                                s, &row
                            )
                        })?;
                        Ok((Direction::In, amt.abs()))
                    }
                    (String(in_), String(out)) => {
                        let empty_in = in_.trim().is_empty();
                        let empty_out = out.trim().is_empty();
                        if empty_in && empty_out {
                            unimplemented!("return error") // or skip row?
                        } else if empty_in {
                            let amt = config.numbers.parse_amount(out).with_context(|| {
                                format!(
                                    "failed to parse out amount from cell String({}). row={:#?}",
                                    out, &row
                                )
                            })?;
                            Ok((Direction::Out, amt.abs()))
                        } else if empty_out {
                            let amt = config.numbers.parse_amount(out).with_context(|| {
                                format!(
                                    "failed to parse in amount from cell String({}). row={:#?}",
                                    in_, &row
                                )
                            })?;
                            Ok((Direction::In, amt.abs()))
                        } else {
                            unreachable!()
                        }
                    }
                    (Empty, Float(f)) => {
                        let amt = BigDecimal::try_from(*f)?;
                        Ok((Direction::Out, amt.abs()))
                    }
                    (Float(f), Empty) => {
                        let amt = BigDecimal::try_from(*f)?;
                        Ok((Direction::In, amt.abs()))
                    }
                    (Empty, Int(i)) => {
                        let amt = BigDecimal::from(*i);
                        Ok((Direction::Out, amt.abs()))
                    }
                    (Int(i), Empty) => {
                        let amt = BigDecimal::from(*i);
                        Ok((Direction::In, amt.abs()))
                    }
                    (in_, out) => unimplemented!("in cell={:#?} out cell={:#?}", in_, out),
                }
            }
        }
    }

    fn parse_description(config: &Config, row: &[DataType]) -> Result<String> {
        let col_idx = config.xlsx.description_column;
        let cell = &row[col_idx];
        if cell.is_empty() {
            Err(Error::msg(format!(
                "empty description cell. col_idx={}; row={:#?}",
                col_idx, &row
            )))
        } else {
            let str = cell.get_string().with_context(|| {
                format!("could not read cell {:?} as string. row={:#?}", cell, &row)
            })?;
            let str = SPACE_RE.replace_all(str, " "); // squash repeated spaces
            Ok(str.to_string())
        }
    }

    fn is_in(&self) -> bool {
        Direction::In == self.direction
    }

    fn is_out(&self) -> bool {
        Direction::Out == self.direction
    }
}

struct CategorizedAmounts<'a> {
    ins: HashMap<String, Vec<&'a CategorizedAmount<'a>>>,
    outs: HashMap<String, Vec<&'a CategorizedAmount<'a>>>,
}

//
impl<'a> From<&'a [CategorizedAmount<'_>]> for CategorizedAmounts<'a> {
    fn from(cas: &'a [CategorizedAmount<'_>]) -> Self {
        let ins = cas
            .iter()
            .filter(|ca| ca.is_in())
            .into_group_map_by(|ca| ca.category.name.to_string());

        let outs = cas
            .iter()
            .filter(|ca| ca.is_out())
            .into_group_map_by(|ca| ca.category.name.to_string());

        CategorizedAmounts { ins, outs }
    }
}

impl<'a> CategorizedAmounts<'_> {
    /// call `.render()` to draw
    pub fn build_table(categorized_amounts: &'a [CategorizedAmount]) -> term_table::Table<'a> {
        use term_table::row::Row;
        use term_table::{Table, TableStyle};

        let mut table = Table::new();
        table.style = TableStyle::thin();

        table.add_row(Row::new(vec![
            TableCell::new("amount"),
            TableCell::new("category"),
            TableCell::new("description"),
        ]));

        for ca in categorized_amounts {
            table.add_row(Row::new(vec![
                TableCell::new(&ca.amount),
                TableCell::new(&ca.category.name),
                TableCell::new(&ca.description),
            ]))
        }

        table
    }

    pub fn build_pie_chart_data(&self) -> Vec<Data> {
        use piechart::Color;
        use rand::Rng;
        let mut rng = rand::thread_rng();

        let mut rand_color = || {
            let n = rng.gen_range(0..=255);
            Some(Color::Fixed(n))
        };

        let mut totals = HashMap::new();
        for (category_name, cas) in &self.outs {
            let tot: BigDecimal = cas.iter().map(|ca| &ca.amount).sum();
            totals.insert(category_name, tot);
        }
        totals.remove(&"ignored".to_string());

        let mut data = Vec::with_capacity(self.outs.len());
        for (lbl, tot) in totals {
            let d = Data {
                label: lbl.clone(),
                value: tot.to_f32().unwrap(),
                color: rand_color(),
                fill: '•',
            };
            data.push(d);
        }

        data
    }
}
